
export class SigninObject {

    public email: string;
    public name: string;
    public document_type: string;
    public document_number: string;
    public password: string;
  
    constructor( object: any){
      this.email = (object.email) ? object.email : null;
      this.name = (object.name) ? object.name : null;
      this.document_number = (object.document_number) ? object.document_number : null;
      this.document_type = (object.document_type) ? object.document_type : null;
      this.password = (object.password) ? object.password : null;
    }
  }
  