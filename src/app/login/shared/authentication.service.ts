import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {LoginObject} from "./login-object.model";
import {SigninObject} from "./signin-object.model";

import {Session} from "../../core/models/session.model";

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) {}

  private basePath = 'http://pruebamerqueo:8888/api/';

  login(loginObj: LoginObject): Observable<Session> {
    return this.http.post(this.basePath + 'login', loginObj).map(this.extractData);
  }

  signin(signinObj: SigninObject): Observable<Session> {
    return this.http.post(this.basePath + 'signin', signinObj).map(this.extractData);
  }

  logout(): Observable<Boolean> {
    return this.http.post(this.basePath + 'logout', {}).map(this.extractData);
  }

  private extractData(res: Response) {
    console.log(res.json());
    let body = res.json();
    return body;
  }
}
