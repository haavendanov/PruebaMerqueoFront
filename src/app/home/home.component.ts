
import {Component} from "@angular/core";
import {Validators, FormGroup, FormBuilder} from "@angular/forms";
import {StorageService} from "../core/services/storage.service";
import {User} from "../core/models/user.model";
import {AuthenticationService} from "../login/shared/authentication.service";
import {DataService} from "./data.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class HomeComponent {
  public user: User;
  public fileForm: FormGroup;

  constructor(
    private storageService: StorageService,
    private authenticationService: AuthenticationService,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private http: Http
  ) { }

  ngOnInit() {
    this.user = this.storageService.getCurrentUser();
    this.fileForm = this.formBuilder.group({
      file: ['', Validators.required],
    })
  }

  public logout(): void{
    this.authenticationService.logout().subscribe(
        response => {if(response) {this.storageService.logout();}}
    );
  }

  public submitFile(): void {
    if(this.fileForm.valid){
      this.dataService.submitFile(this.fileForm.value).subscribe(
      )
    }
  }

}

