import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";


@Injectable()
export class DataService {

  constructor(private http: Http) {}

  private basePath = 'http://pruebamerqueo:8888/api/';

  submitFile(file: File): Observable<boolean> {
    return this.http.post(this.basePath + 'submitFile', file).map(this.extractData);
  }

  private extractData(res: Response) {
    console.log(res.json());
    let body = res.json();
    return body;
  }
}
